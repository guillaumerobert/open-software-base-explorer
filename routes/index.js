'use strict';

const express = require('express');
const wrap = require('co-express');

const repository = require('../repository');

var router = new express.Router();

router.use((req, res, next) => {
  res.locals.baseLocation += req.baseUrl;

  return next();
});

router.get('/', (req, res) => {
  if (req.baseUrl === '/api') {
    return res.json({
      version: require('../package.json').version,
      programsUrl: res.locals.baseLocation + '/programs'
    });
  }

  return res.redirect(req.baseUrl + '/programs');
});

router.get('/programs', wrap(function * (req, res) {
  var programs = yield repository.get();

  if (req.baseUrl === '/api') {
    var data = programs.map(program => ({
      name: program.name,
      url: res.locals.baseLocation + '/programs/' + program.name
    }));
    return res.json(data);
  }

  return res.render('index', {programs: programs, template: 'index'});
}));

router.get('/programs/:name', wrap(function * (req, res, next) {
  var data;
  try {
    data = yield repository.get(req.params.name);
  } catch (err) {
    return next(err);
  }

  if (req.baseUrl === '/api') {
    return res.json(data);
  }

  return res.render('program', {program: data, template: 'program'});
}));

router.get('/programs/search/:string', wrap(function * (req, res) {
  var programs = yield repository.get();

  programs = programs.filter(program => (
    program.name.search(new RegExp(req.params.string, 'gi')) !== -1
  ));

  if (req.baseUrl === '/api') {
    var data = programs.map(program => ({
      name: program.name,
      url: res.locals.baseLocation + '/programs/' + program.name
    }));
    return res.json(data);
  }

  return res.render('index', {
    programs: programs,
    template: 'index',
    search: req.params.string
  });
}));

module.exports = router;
