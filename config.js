'use strict';

module.exports = {
  port: 3000,
  locales: ['fr', 'en', 'es']
};
