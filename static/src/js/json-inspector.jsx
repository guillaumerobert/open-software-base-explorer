/* eslint-env browser */
'use strict';

const React = require('react');
const ReactDOM = require('react-dom');

var Inspector = require('react-json-inspector');
require('react-json-inspector/json-inspector.css');
require('../styles/json-inspector.css');

Inspector.load = (id, data) => {
  ReactDOM.render(
    <Inspector data={data} />,
    document.getElementById(id)
  );
};

module.exports = Inspector;
